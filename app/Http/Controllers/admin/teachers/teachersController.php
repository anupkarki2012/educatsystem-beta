<?php

namespace App\Http\Controllers\admin\teachers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Faculty;
Use Alert;


class teachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data['teachers']=Teacher::all();

     

        return view('admin.teachers.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['faculty']=Faculty::all();
        return view('admin.teachers.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'faculty_id'=>'required',
        'teacher_name'=>'required',
        'teacher_status'=>'required',
        'teacher_email'=> 'required',
        'teacher_address'=> 'required',
        'contact_number'=>'required',
        'subject'=>'required',
        'join_year'=>'required',
        'gender'=>'required',
        't_description'=>'required',
       
        'upload_image'=>'required',
     ]);
    
         if($request->hasFile('upload_image'))
             { 
                $file=$request->file('upload_image');
                $extension=$file->getClientOriginalExtension();
                $filename=time().'.'.$extension;
                $file->move('Teachers/profile/img/',$filename); 
             }
Teacher::Create($request->only(['faculty_id','teacher_name','teacher_status','teacher_email','teacher_address','contact_number','subject',
    'join_year','gender','t_description','upload_image']));
        Alert::success('Success', 'Teacher has been added successfully');
        return redirect(route('admin.teachers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
     {
        Teacher::where('id',$id)->delete();
        Alert::success('Success Title', 'Teacher has been deleted');
         return redirect(route('admin.teachers.index'));

    }
}
