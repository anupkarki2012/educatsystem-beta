<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Student;
use App\Assignment;
use App\ProgramCourse;
use App\Notice;
use App\Faculty;
use App\Program;
use App\Marks;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    public function edit(Request $request , $id)
    {
        $rules=['image'=>'image|required'
        ];

      $this->validate($request,$rules);
      
          if($request->hasFile('image'))

                     {
                       if(Auth::user()->image)
                       {
                       unlink(public_path().'/User/profile/img/'.Auth::user()->image);
                       }
                      }

            if($request->hasFile('image'))
             {  
                $file=$request->file('image');
                $extension=$file->getClientOriginalExtension();
                $filename=time().'.'.$extension;
                $file->move('User/profile/img/',$filename);
                $data['image']=$filename;
             }
        User::where('id',$id)->update($data);
        return redirect()->back();
    }
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard.home');
    }


    public static function student()
    {
         return  Student::count();
        


    }
    public static function assignment()
    {
        return Assignment::count();
    }

    public static function notice()
    {
           return Notice::count();
        
    }

    public static function programCourse()
    {
        return ProgramCourse::count();

    }

    public static function faculty()
    {
         return Faculty::count();
    }
    
    public static function program()
    {
        return Program::count();
    }
   


    public static function marks()
    {
        return Marks::count();
    }

}
